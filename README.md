# Docker Example App

LAMP стек для быстрой установки PHP-приложений с помощью Docker. См. [отдельную статью](https://php.dragomano.ru/docker-kak-zamena-open-server).

## Ингредиенты

- PHP 8.3 + Apache
- MariaDB
- Adminer

Переменные окружения см. в файле `.env.example` в корне проекта. Не забудьте переименовать его в `.env`

## Как готовить

Посолить, поперчить, в отдельную папку положить, открыть консоль в этой папке и волшебные слова пробормотать:

```sh
docker compose up -d
```
